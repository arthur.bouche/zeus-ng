#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QJsonObject>

class Config {
public:
    explicit Config();
    bool load(QString filepath);
    void show();

    QString getBrokerAdress();
    int getBrokerPort();
	QString getWorkingDirectory();
    QString getAppsUrl();
    int getAppsPeriodDownload();
    bool getAppsOneShot();
    QString getAppsMode();

private:
    QJsonObject m_json;
};

#endif // CONFIG_H
