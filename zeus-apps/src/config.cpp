#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QTextStream>
#include "config.h"
#include "debug.h"

Config::Config()
{
}

bool Config::load(QString filepath)
{
    bool rc;

    qStdout() << "Using configuration file:" << filepath << Qt::endl;

    /*
     *  Open the file
     */
    QFile file(filepath);
    rc = file.open(QFile::ReadOnly);
    if (rc == false) {
        qStdout() << "The configuration file is not readable" << Qt::endl;
        return false;
    }

    /*
     *  Initilize Json Document
     */
    QString content = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(content.toUtf8());
    if (doc.isNull()) {
        qStdout() << "The configuration file is not JSON valid" << Qt::endl;
        return false;
    }

    /*
     *  Ensure the configuration file is an object
     */
    if (doc.isObject() == false) {
        qStdout() << "The configuration file is not a JSON Object" << Qt::endl;
        return false;
    }
    m_json = doc.object();

    return true;
}

void Config::show()
{
    qStdout() << "Configuration summary" << Qt::endl;
    qStdout() << "\tbroker : "  << Qt::endl;
    qStdout() << "\t\taddress : " << getBrokerAdress() << Qt::endl;
    qStdout() << "\t\tport : " << getBrokerPort() <<Qt::endl;
    qStdout() << "\tapps : " << Qt::endl;
    qStdout() << "\t\tWorking Directory : " << getWorkingDirectory() << Qt::endl;
    qStdout() << "\t\turl : "<< getAppsUrl()  << Qt::endl;
    qStdout() << "\t\tperiodDownload : " << getAppsPeriodDownload() << Qt::endl;
    qStdout() << "\t\tAppsOneShot : " << getAppsOneShot() << Qt::endl;
    qStdout() << "\t\tAppMode : " << getAppsMode() << Qt::endl;

}

QString Config::getBrokerAdress()
{
    QJsonValue value = m_json["broker"];
    QJsonObject object = value.toObject();
    return object["address"].toString();
}

int Config::getBrokerPort()
{
    QJsonValue value = m_json["broker"];
    QJsonObject object = value.toObject();
    return object["port"].toInt();
}

QString Config::getWorkingDirectory()
{
	QJsonValue value = m_json["apps"];
	QJsonObject object = value.toObject();
	return object["WorkingDirectory"].toString();
}

QString Config::getAppsUrl()
{
    QJsonValue value = m_json["apps"];
    QJsonObject object = value.toObject();
	return object["url"].toString();
}

int Config::getAppsPeriodDownload()
{
    QJsonValue value = m_json["apps"];
    QJsonObject object = value.toObject();
    return object["periodDownload"].toInt();
}

bool Config::getAppsOneShot()
{
    QJsonValue value = m_json["apps"];
    QJsonObject object = value.toObject();
    return object["oneShot"].toBool();
}

QString Config::getAppsMode()
{
    QJsonValue value = m_json["apps"];
    QJsonObject object = value.toObject();
    return object["AppMode"].toString();
}


