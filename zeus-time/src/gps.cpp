#include "gps.h"
#include "ubx.h"
#include "debug.h"

#include <sys/ioctl.h>

const QString GPS::fixedFilePath = QStringLiteral("/etc/gps-fixed-position.ecef");

GPS::GPS(QJsonObject config, bool bNTP=false, QObject *parent) :
	QObject(parent)
{
	m_dateValid = false;
	m_config = config;
	m_ntp = bNTP;
}

bool GPS::isSurveyingFinished(){
	if(m_gpsdata.isFixedMode == true && m_gpsdata.isSurveyinMode == false){
		return true;
	}else{
		return false;
	}
}

void GPS::checkFixedStatus(){
	if(isSurveyingFinished()){
		//lancer la création du fichier /etc/gps-fixed-position.ecef
        qStdout() << "Passage en mode fixe, creation du fichier ecef" << Qt::endl;;
		m_ubx->updateFixedConfiguration(m_gpsdata.ECEFMeanX , m_gpsdata.ECEFMeanY, m_gpsdata.ECEFMeanZ, m_gpsdata.ECEFMeanV);
		m_ubx->enterFixedMode();
		return;
	}else{
        m_waitingForFixedMode->start(10 * 60 * 1000); //toutes les minutes
        qStdout() << "Survey mode en cours" << Qt::endl;;
		return;
	}
	return;
}

bool GPS::start()
{
	// Open and configure the GPS receiver
    m_uart = new QSerialPort("/dev/ttySTM7", this);
	m_waitingForFixedMode = new QTimer(this);
	m_waitingForFixedMode->setTimerType(Qt::VeryCoarseTimer);
	connect(m_uart, &QSerialPort::readyRead, this, &GPS::uartReadyRead);
	connect(m_waitingForFixedMode, SIGNAL(timeout()), this, SLOT(checkFixedStatus()));

	if (m_uart->open(QSerialPort::ReadWrite) == false) {
		return false;
	}

	m_ubx = new Ublox(m_uart, this);
	m_ubx->setSurveyDuration(surveyDuration());
	m_ubx->setSurveyAccuracy(surveyAccuracy());
	m_ubx->configureReceiver();

	if(m_ntp==false){
		// Enter in fixed mode if configured
		QFileInfo fileinfo(GPS::fixedFilePath);
		if (isFixedMode() || fileinfo.exists()) {
			if(m_ubx->enterFixedMode() == false){
				m_waitingForFixedMode->setTimerType(Qt::VeryCoarseTimer);//permet de claquer le timer plutôt si QT se rend compte du besoin
				m_waitingForFixedMode->start(1 * 60 * 1000); //toutes les minutes
                qStdout() << __FUNCTION__ << "Started QTimer for surveying mode" << Qt::endl;;
			}else{
                qStdout() << __FUNCTION__ << "GPS already fixed" << Qt::endl;;
			}
		}
		m_ubx->antennaAndPPSConfiguration();
	}
	m_uart->flush();
	connect(m_uart, SIGNAL(readyRead()), this, SLOT(uartReadyRead()));

	// Publish the GPS Uart on TCP
	m_forward = new GPSServer(this);
	connect(m_forward, SIGNAL(dataAvailable(QByteArray)), this, SLOT(tcpDataAvailable(QByteArray)));
	m_forward->start();

	if(m_ntp==false){
		// Every 1min, then 1h update the current position file
		m_positionFileTimer = new QTimer(this);
		m_positionFileTimer->setTimerType(Qt::VeryCoarseTimer);
		m_positionFileTimer->start(1 * 60 * 1000);
		connect(m_positionFileTimer, SIGNAL(timeout()), this, SLOT(createPositionFile()));
	}

	if(m_ntp==false){
		// Every 15min update the system time
		m_syncDateTimer = new QTimer(this);
		m_syncDateTimer->setTimerType(Qt::VeryCoarseTimer);
		m_syncDateTimer->start(1 * 60 * 1000);
		connect(m_syncDateTimer, SIGNAL(timeout()), this, SLOT(syncDate()));
	}

	// IFSTTAR driver endpoint
	m_driver = new QFile("/dev/pegase-sync-drv");

	return true;
}

void GPS::syncDate()
{
	// Ensure the driver is available
	int fd = getDriverFd();
	if (fd == -1) {
		qDebug() << "getDriverFd failed";
		return;
	}

	// Ensure date is valid
	if (m_dateValid == false) {
		qDebug() << "Date is not valid yet, do not sync system date";
		m_syncDateTimer->setInterval(1 * 60 * 1000);
		return;
	}

	// Ensure period is 15min
	m_syncDateTimer->setInterval(15 * 60 * 1000);

	// Ask driver to sync the date
	int rc = ioctl (fd, SYNC_IOCTL_SET_SYSTEMTIME, NULL);
	if (rc != 0) {
        qStdout() << "SYNC_IOCTL_SET_SYSTEMTIME failed" << Qt::endl;;
	}

	qDebug().nospace() << "Sync system date : "
					   << m_gpsdata.uiDay << "/" << m_gpsdata.uiMonth << "/" << m_gpsdata.uiYear << " "
					   << m_gpsdata.uiHour << "h" << m_gpsdata.uiMinute << "m" << m_gpsdata.uiSeconde << "s";

}

void GPS::uartReadyRead()
{
	do {
		QByteArray data = m_uart->readAll();
		if (data.size() == 0) {
			break;
		}

		// Forward incomming data to the TCP server
		if (m_forward != Q_NULLPTR) {
			m_forward->sendData(data);
		}

		// Process data
		parse(data);
	} while(m_uart->bytesAvailable());
}

void GPS::tcpDataAvailable(QByteArray payload)
{
	qDebug() << payload.toHex();

	if (m_uart != Q_NULLPTR) {
		m_uart->write(payload);
	}
}

void GPS::createPositionFile()
{
	// Update interval
	m_positionFileTimer->stop();
	m_positionFileTimer->start(positionFilePeriod() * 1000);

	// Generate the JSON document
	QJsonObject position;
	position.insert("x", QJsonValue(m_gpsdata.dLatitude));
	position.insert("y", QJsonValue(m_gpsdata.dLongitude));
	position.insert("z", QJsonValue(m_gpsdata.dAltitude));
	position.insert("accuracy", QJsonValue((double)m_gpsdata.uiAcc));

	QJsonArray satellites;
	for (unsigned int satIndex=0; satIndex<m_gpsdata.uiNumCh; satIndex++) {
		QJsonObject sat;
		sat.insert("id", (double)m_gpsdata.sv[satIndex].uiIdSat);
		sat.insert("quality", (double)m_gpsdata.sv[satIndex].uiQuality);
		sat.insert("elevation", m_gpsdata.sv[satIndex].iElev);
		sat.insert("azimuth", m_gpsdata.sv[satIndex].iAzim);
		satellites.push_back(sat);
	}

	QJsonObject root;
	root.insert("ts", QJsonValue((double)m_gpsdata.oTimeTTimeInSeconds));
	root.insert("tow", QJsonValue((double)m_gpsdata.uiTow));
	root.insert("satteliteNavSolution", QJsonValue((double)m_gpsdata.uiNumSV));
	root.insert("position", position);
	root.insert("satellites", satellites);

	QJsonDocument doc(root);
	QByteArray json = doc.toJson();

	// Write JSON on disk
	QString filepath = positionFileFolder();
	filepath.append("/position.json");

	QFile file(filepath);
	if (file.open(QFile::WriteOnly) == false) {
		return;
	}
	file.write(json);
	file.close();
	file.rename("/lcpc/upload/position.gps");
}

bool GPS::isFixedMode() const
{
	return m_config["fixed"].toBool(false);
}

QString GPS::modelGPS() const
{
	return m_config["model"].toString();
}

int GPS::antennaLength() const
{
	return m_config["antennaLength"].toInt();
}

int GPS::antennaVelocity() const
{
	return m_config["antennaVelocity"].toInt();
}

int GPS::TImeIntervalPPS() const
{
	return m_config["TImeIntervalPPS"].toInt();
}

int GPS::LengthPPS() const
{
	return m_config["LengthPPS"].toInt();
}

int GPS::surveyDuration() const
{
	QJsonObject survey = m_config["survey"].toObject();
	return survey["duration"].toInt();
}

int GPS::surveyAccuracy() const
{
	QJsonObject survey = m_config["survey"].toObject();
	return survey["accuracy"].toInt();
}

int GPS::positionFilePeriod() const
{
	QJsonObject positionFile = m_config["positionFile"].toObject();
	return positionFile["period"].toInt();
}

QString GPS::positionFileFolder() const
{
	QJsonObject positionFile = m_config["positionFile"].toObject();
	return positionFile["folder"].toString();
}

uint8_t GPS::hex2int(char c) const
{
	if (c >= '0' && c <= '9') {
		return c - '0';
	}

	if (c >= 'a' && c <= 'f') {
		return c - 'a' + 10;
	}

	if (c >= 'A' && c <= 'F') {
		return c - 'A' + 10;
	}

	return 0;
}

void GPS::parse(QByteArray payload)
{
	static char buffer[4096];
	static int buffer_index;
	static int state = 0;
	static bool isInit = false;
	static uint16_t ubx_id;
	static int ubx_len;
	static uint8_t ubx_chk_a, ubx_chk_b;
	static uint8_t nmea_chk;
	static uint8_t nmea_chk_valid;

	if (isInit == false) {
		isInit = true;
		buffer_index = 0;
	}

	int i;
	for (i=0; i<payload.size(); i++) {

		// Check buffer
		if (buffer_index == sizeof(buffer)) {
			state = 0;
		}

		// Wait start
		if (state == 0) {
			if (payload[i] == '$') {
				state = 10;
				nmea_chk = 0;
				buffer[0] = payload[i];
				buffer_index = 1;
			} else
				if (payload[i] == (char)0xB5) {
					state = 20;
					ubx_chk_a = 0;
					ubx_chk_b = 0;
					buffer_index = 0;
				}
		} else

			// 2nd start for ubx
			if (state == 20) {
				if (payload[i] == (char)0x62) {
					state = 21;
				} else {
					state = 0;
				}

			} else

				// Ubx header
				if (state == 21) {
					buffer[buffer_index] = payload[i];
					buffer_index++;

					ubx_chk_a = ubx_chk_a + payload[i];
					ubx_chk_b = ubx_chk_b + ubx_chk_a;

					if (buffer_index == 4) {
						ubx_id = buffer[1] + buffer[0] * 256;
						ubx_len = buffer[2] + buffer[3] * 256;
						state = 22;
					}
				} else

					// Ubx payload
					if (state == 22) {
						buffer[buffer_index] = payload[i];
						buffer_index++;

						ubx_chk_a = ubx_chk_a + payload[i];
						ubx_chk_b = ubx_chk_b + ubx_chk_a;

						if (buffer_index == 4 + ubx_len) {
							state = 23;
						}
					} else

						// Ubx chk 1
						if (state == 23) {
							buffer[buffer_index] = payload[i];
							buffer_index++;
							state = 24;
						} else

							// Ubx chk 2
							if (state == 24) {
								buffer[buffer_index] = payload[i];
								buffer_index++;

								if (buffer[buffer_index - 1] == ubx_chk_b && buffer[buffer_index - 2] == ubx_chk_a) {
									QByteArray ubxPayload(buffer + 4, buffer_index - 4);
									ubxDecode(ubx_id, ubxPayload);
								} else {
									qDebug() << "UBX Invalid Checksum";
								}

								state = 0;
							} else

								// Read NMEA
								if (state == 10) {
									buffer[buffer_index] = payload[i];
									buffer_index++;

									if (payload[i] == '*') {
										state = 11;
									} else {
										nmea_chk = nmea_chk ^ payload[i];
									}
								} else

									// Wait NMEA CHK 1
									if (state == 11) {
										buffer[buffer_index] = payload[i];
										buffer_index++;
										state = 12;
									} else

										// Wait NMEA CHK 2
										if (state == 12) {
											buffer[buffer_index] = payload[i];
											buffer_index++;
											state = 13;

											nmea_chk_valid = 16 * hex2int(buffer[buffer_index - 2]) + hex2int(buffer[buffer_index - 1]);
										} else

											// Wait NMEA EOL
											if (state == 13) {
												buffer[buffer_index] = payload[i];
												buffer_index++;

												if (payload[i] == (char) 0x0A) {
													state = 0;
													buffer[buffer_index] = '\0';
													buffer_index++;

													if (nmea_chk == nmea_chk_valid) {
														nmeaDecode(QByteArray(buffer, buffer_index));
													} else {
														qDebug() << "NMEA Invalid Checksum";
													}

													buffer_index = 0;
												}
											}
	}
}

void GPS::updateUnixTimestamp()
{
	struct tm t;
    static int i = 0;
    static bool firstTime = true;
	t.tm_year = m_gpsdata.uiYear - 1900;
	t.tm_mon = m_gpsdata.uiMonth - 1;
	t.tm_mday = m_gpsdata.uiDay;
	t.tm_hour = m_gpsdata.uiHour;
	t.tm_min = m_gpsdata.uiMinute;
	t.tm_sec = m_gpsdata.uiSeconde;


	m_gpsdata.oTimeTTimeInSeconds = mktime(&t);
    if(firstTime){
     if(m_gpsdata.uiYear >= 2017) {
         firstTime = false;
     }
     qDebug() << "ioctl "<< i << " : " << m_gpsdata.uiYear;
     i++;
    }
     /*
	 * Append a seconds, NMEA data are sent just after the PPS
	 * Then update others fields
	 *
	 *  See Ublox documentation : Page 26 of 208
	 */
	m_gpsdata.oTimeTTimeInSeconds += 1;
    //TODO attention au cast
    gmtime_r((const time_t*)&(m_gpsdata.oTimeTTimeInSeconds), &t);
	m_gpsdata.uiYear = t.tm_year + 1900;
	m_gpsdata.uiMonth = t.tm_mon + 1;
	m_gpsdata.uiDay = t.tm_mday;
	m_gpsdata.uiHour = t.tm_hour;
	m_gpsdata.uiMinute = t.tm_min;
	m_gpsdata.uiSeconde = t.tm_sec;



	// Push decoded data into driver
	int fd = getDriverFd();
	if (fd == -1) {
		qDebug() << "getDriverFd failed";
		return;
	}
    //qDebug() << "ioctl " << m_gpsdata.uiYear;
	if (m_gpsdata.uiYear >= 2017) {

		int rc = ioctl (fd, SYNC_IOCTL_SET_GPS_DATA, &m_gpsdata);
        if (rc != 0) {
			qDebug() << "IOCTL failed";
		}
	}
}

int GPS::getDriverFd()
{
	if (m_driver->isOpen()) {
		return m_driver->handle();
	}

	if (m_driver->open(QFile::ReadWrite) == false) {
		return -1;
	}

	return m_driver->handle();
}

void GPS::ubxDecode(uint16_t ubx_id, QByteArray payload)
{
	bool sync = true;
	union ubx *data = (union ubx *)payload.data();

	switch(ubx_id) {
	case TIM_TP:
		m_gpsdata.qErr = data->tim_tp.qErr;
		break;

	case TIM_SVIN:
		m_gpsdata.isSurveyinMode = data->tim_svin.active;
		m_gpsdata.surveyinObservationTime = data->tim_svin.dur;
		m_gpsdata.ECEFMeanX = data->tim_svin.meanX;
		m_gpsdata.ECEFMeanY = data->tim_svin.meanY;
		m_gpsdata.ECEFMeanZ = data->tim_svin.meanZ;
		m_gpsdata.ECEFMeanV = data->tim_svin.meanV;
		break;

	case NAV_STATUS:
		m_gpsdata.isFixedMode = (data->nav_status.gpsFix == NAV_STATUS_GPSFIX_TIME);
		break;

	case NAV_TIMEUTC:
		m_gpsdata.timeAccuracy = data->nav_timeutc.tAcc;
		m_dateValid = data->nav_timeutc.valid & NAV_TIMEUTC_VALID_UTC;
		break;

	case NAV_SOL:
		m_gpsdata.uiTow   = data->nav_sol.iTOW;
		m_gpsdata.uiAcc   = data->nav_sol.sAcc;
		m_gpsdata.uiNumSV = data->nav_sol.numSV;
		if (m_gpsdata.uiNumSV == 0) {
			m_gpsdata.qErr = 0;
		}
		break;

	case NAV_SVINFO:
		m_gpsdata.uiNumCh = data->nav_svinfo.numCh;
		for(unsigned int i=0; i<m_gpsdata.uiNumCh; i++) {
			m_gpsdata.sv[i].uiIdSat   = data->nav_svinfo.sv[i].svid;
			m_gpsdata.sv[i].uiQuality = data->nav_svinfo.sv[i].quality;
			m_gpsdata.sv[i].iElev 	= data->nav_svinfo.sv[i].elev;
			m_gpsdata.sv[i].iAzim 	= data->nav_svinfo.sv[i].azim;
		}
		break;

	default:
		sync = true;
		break;
	}

	if (sync) {
		updateUnixTimestamp();
	}
}

void GPS::nmeaDecode(QByteArray payload)
{
	if (payload.startsWith("$GPGGA") || payload.startsWith("$GNGGA")) {
		nmeaDecodeGGA(payload);
	} else if (payload.startsWith("$GPGSA") || payload.startsWith("$GNGSA")) {
		nmeaDecodeGSA(payload);
	} else if (payload.startsWith("$GPZDA") || payload.startsWith("$GNZDA")) {
		nmeaDecodeZDA(payload);
	}
}

void GPS::nmeaDecodeGGA(QByteArray payload)
{
	int ent;
	float dec;

	QList<QByteArray> chunks = payload.split(',');
	if (chunks.size() > 8) {

		int uiTime = (int)chunks[1].toDouble();
		m_gpsdata.uiHour = uiTime / 10000;
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.uiHour << Qt::endl;;
		m_gpsdata.uiMinute = (uiTime / 100) % 100;
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.uiMinute << Qt::endl;;
		m_gpsdata.uiSeconde = uiTime % 100;
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.uiSeconde << Qt::endl;;

		m_gpsdata.dLatitude = chunks[2].toFloat();
		ent = floor(m_gpsdata.dLatitude / 100);
		dec = (m_gpsdata.dLatitude / 100 - ent) * 100;
		m_gpsdata.dLatitude = ent + dec / 60;

		if (chunks[3].at(0) == 'S') {
			m_gpsdata.dLatitude = -1.0 * m_gpsdata.dLatitude;
		}
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.dLatitude << Qt::endl;;

		m_gpsdata.dLongitude = chunks[4].toFloat();
		ent = floor(m_gpsdata.dLongitude / 100);
		dec = (m_gpsdata.dLongitude / 100 - ent) * 100;
		m_gpsdata.dLongitude = ent + dec / 60;

		if (chunks[5].at(0) == 'W') {
			m_gpsdata.dLongitude = -1.0 * m_gpsdata.dLongitude;
		}
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.dLongitude << Qt::endl;;

		m_gpsdata.iQuality = chunks[6].toInt();
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.iQuality << Qt::endl;;
		m_gpsdata.uiSatelliteUsed = chunks[7].toInt();
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.uiSatelliteUsed << Qt::endl;;
		m_gpsdata.dHdop = chunks[8].toFloat();
        //qStdout() << __FUNCTION__ << " " << m_gpsdata.dHdop << Qt::Qt::endl;;;
	}
}

void GPS::nmeaDecodeGSA(QByteArray payload)
{
	QList<QByteArray> chunks = payload.split(',');
	if (chunks.size() > 17) {
		m_gpsdata.dVdop = chunks[17].toFloat();
	}
}

void GPS::nmeaDecodeZDA(QByteArray payload)
{
	QList<QByteArray> chunks = payload.split(',');
	if (chunks.size() > 4) {
		m_gpsdata.uiDay = chunks[2].toInt();
		m_gpsdata.uiMonth = chunks[3].toInt();
		m_gpsdata.uiYear = chunks[4].toInt();

		updateUnixTimestamp();
	}
}
