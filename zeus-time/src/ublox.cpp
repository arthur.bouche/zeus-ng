#include <QFileInfo>
#include <QFile>
#include <QThread>
#include <QDebug>
#include "ublox.h"
#include "debug.h"
#include "gps.h"
#include "config.h"

const QString Ublox::fixedFilePath = QStringLiteral("/etc/gps-fixed-position.ecef");

Ublox::Ublox(QSerialPort *device, QObject *parent) :
	QObject(parent)
{
	m_device = device;
	m_duration = 2 * 24 * 3600;
	m_accuracy = 1000;
}

void Ublox::setDeviceBaudrate(qint32 baudRate)
{
	if (m_device->isOpen()) {
		m_device->close();
		m_device->setBaudRate(baudRate);
		m_device->open(QIODevice::ReadWrite);
	} else {
		m_device->setBaudRate(baudRate);
	}
}

void Ublox::configureReceiver()
{

	bool rc;
	Config conf;
	GPS *gps;
	rc = conf.load("/etc/zeus/time.json");
	if (rc == false) {
        qStdout() << "Fallback on build-in configuration" << Qt::endl;
		return;
		rc = conf.load(":/time.json");
		if (rc == false) {
            qStdout() << "Can not load configuration" << Qt::endl;
			return;
		}
	}
	/*
	 *  Initialize time source
	 */
	QString source = conf.getTimeSource();
	if (source == "gps" || source == "ntp") {
        gps = new GPS(conf.getTimeConfig(), false);
		// FFS connect zeus bus
	}
    qStdout() << "Reset GPS configuration" << Qt::endl;

	// Send UBX reset at 115200 baud
	setDeviceBaudrate(115200);
	resetConfiguration();
	QThread::sleep(1);

	// Send UBX reset at 9600 baud
	setDeviceBaudrate(9600);
	resetConfiguration();
	QThread::sleep(1);


    qStdout() << "Upgrade GPS baudrate to 115200" << Qt::endl;

	// Configure baudrate from 9600 to 115200
	setDeviceBaudrate(9600);
	setBaudrate(115200);
	QThread::sleep(1);
	setDeviceBaudrate(115200);

	// Perform user configuration
	if(gps->modelGPS()=="neo-6t"){
        qStdout() << "Applying 6T configuration" << Qt::endl;
		applyConfigurationFile("/etc/zeus/ublox_neo_6t.conf");
	}else if(gps->modelGPS()=="neo-8t"){
        qStdout() << "Applying 8T configuration" << Qt::endl;
		applyConfigurationFile("/etc/zeus/ublox_neo_8t.conf");
	}else{
		applyConfigurationFile("/etc/zeus/ublox.conf");
	}
	if(conf.getNTPSource()=="gps"){
		//Copier fichier basé sur fournir l'heure via le GPS au serveur NTP
		system("cp /etc/zeus/ntp_sourceGPS.conf /etc/ntp.conf");
	}else if(conf.getNTPSource()=="network"){
		//Copier fichier basé sur fournir l'heure via internet au serveur NTP
		system("cp /etc/zeus/ntp_sourceNTP.conf /etc/ntp.conf");
	}
	delete(gps);
}

void Ublox::setSurveyDuration(unsigned int duration)
{
	m_duration = duration;
}

void Ublox::setSurveyAccuracy(unsigned int accuracy)
{
	m_accuracy = accuracy;
}

/*
 * Reload the default configuration except CFG-ANT
 */
void Ublox::resetConfiguration()
{
	UbloxFrame payload;

	payload.appendHeader();
	payload.append(0x06);
	payload.append(0x09);
	payload.append(0x0D);
	payload.append(0x00);
	payload.append(0xFF);
	payload.append(0xFB);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0xFF);
	payload.append(0xFB);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x03);
	payload.appendChecksum();

	if (m_device != Q_NULLPTR) {
		m_device->write(payload.data(), payload.length());
		m_device->flush();
	}
}

void Ublox::setBaudrate(int baudrate)
{
	UbloxFrame payload;

	payload.appendHeader();
	payload.append(0x06);
	payload.append(0x00);
	payload.append(0x14);
	payload.append(0x00);
	payload.append(0x01);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0xD0);
	payload.append(0x08);
	payload.append(0x00);
	payload.append(0x00);
	payload.appendInt32(baudrate);
	payload.append(0x03);
	payload.append(0x00);
	payload.append(0x03);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.appendChecksum();

	if (m_device != Q_NULLPTR) {
		m_device->write(payload.data(), payload.length());
		m_device->flush();
	}
}

bool Ublox::applyConfigurationFile(QString filepath)
{
	QFile file(filepath);
	if (file.open(QIODevice::ReadOnly) == false) {
        qStdout() << "Can not access to the Ublox configuration file." << Qt::endl;
		return false;
	}

	// Keep only config line starting with "CFG"
	QStringList lines;
	QTextStream stream(&file);
	while(true) {
		if (stream.atEnd()) {
			break;
		}

		QString line = stream.readLine().trimmed();
		if (line.size() == 0) {
			continue;
		}

		if (line.startsWith("CFG") == false) {
			continue;
		}

		lines.append(line);
	}

	// Execute configuration
    qStdout() << "Configuring GPS receiver" << Qt::endl;
	foreach (QString line, lines) {
        qStdout() << "\t" << line << Qt::endl;
		UbloxFrame payload = UbloxFrame::parseUbxLine(line);

		if (m_device != Q_NULLPTR) {
			m_device->write(payload.data(), payload.length());
			QThread::msleep(100);
		}
	}

	return true;
}

bool Ublox::enterFixedMode()
{
	QFileInfo fileinfo(Ublox::fixedFilePath);
	if (fileinfo.exists() == false || fileinfo.isFile() == false) {		// GPS mode fixed NOK
        qStdout() << "No previous position on disk." << Qt::endl;
		enterSurveyInMode();
		return false;
	}

	QFile file(Ublox::fixedFilePath);
	if (file.open(QIODevice::ReadWrite) == false) {					// GPS mode fixed NOK
        qStdout() << "Can not access to previous position on disk." << Qt::endl;
		enterSurveyInMode();
		return false;
	}

	QStringList chuncks;
	QTextStream stream(&file);

	for (int i=0; i<4; i++) {
		QString line = stream.readLine().trimmed();
		if (line.size() > 0) {
			chuncks.append(line);
		}
	}

	if (chuncks.count() < 4) {												// GPS mode fixed NOK
        qStdout() << "Invalid cache for fixed mode" << Qt::endl;
		enterSurveyInMode();
		return false;
	}

	// GPS mode fixed OK

	int x = chuncks.at(0).toInt();
	int y = chuncks.at(1).toInt();
	int z = chuncks.at(2).toInt();
	int v = chuncks.at(3).toInt();

	enterFixedMode(x, y, z, v);
	return true;
}

bool Ublox::enterFixedMode(int x, int y, int z, int v)
{
	bool rc;
	UbloxFrame payload;
	Config conf;
	rc = conf.load("/etc/zeus/time.json");
	if (rc == false) {
        qStdout() << "Fallback on build-in configuration" << Qt::endl;
		return false;
		rc = conf.load(":/time.json");
		if (rc == false) {
            qStdout() << "Can not load configuration" << Qt::endl;
			return false;
		}
    }

    qStdout() << "Enter Fixed mode: x=" << x << "cm , y=" << y  << "cm, z=" << z << "cm, v=" << v << "mm" << Qt::endl;

	payload.appendHeader();
	payload.append(0x06);  // TMODE 2
	payload.append(0x3D);
	payload.append(0x1C);  // 28
	payload.append(0x00);
	payload.append(0x02);  // Fixed mode ON
	payload.append(0x00);  // Reserved
	payload.append(0x00);  // ECEF mode
	payload.append(0x00);
	payload.appendInt32(x);
	payload.appendInt32(y);
	payload.appendInt32(z);
	payload.appendInt32(v);
	payload.appendInt32(0);
	payload.appendInt32(0);
	payload.appendChecksum();

	if (m_device != Q_NULLPTR) {
		m_device->write(payload.data(), payload.length());
	}
	return true;
}

void Ublox::antennaAndPPSConfiguration(void){
	bool rc;
	Config conf;
	GPS *gps;
    UbloxFrame payload;
	int DelayCable=0, VelocityCable=0, AntennaLength=0, iTImeIntervalPPS=1000000, iLengthPPS=100000;
	rc = conf.load("/etc/zeus/time.json");
	if (rc == false) {
        qStdout() << "Fallback on build-in configuration" << Qt::endl;
		return;
		rc = conf.load(":/time.json");
		if (rc == false) {
            qStdout() << "Can not load configuration" << Qt::endl;
			return;
		}
	}
	//*
	 //*  Initialize zeus bus


	//*
	 //*  Initialize time source
    gps = new GPS( conf.getTimeConfig(), false);
	if(gps->modelGPS()=="neo-6t"){
		return;
	}
	//http://manuals.spectracom.com/TS/Content/ASG/AntCableLength.htm
	//D = ( L * C ) / V
	//D = Cable delay in nanoseconds
	//L = Cable length in feet
	//C = Constant derived from velocity of light: 1.016
	//V = Nominal velocity of propagation expressed as decimal, i.e. %66 = 0.66 Value is provided by cable manufacturer.
	AntennaLength=gps->antennaLength();
	VelocityCable=gps->antennaVelocity();
	DelayCable=(AntennaLength*1.016)/VelocityCable;
    qStdout() << "DelayCable : " << DelayCable << Qt::endl;
    qStdout() << "AntennaLength : " << AntennaLength << Qt::endl;
    qStdout() << "VelocityCable : " << VelocityCable << Qt::endl;
	iTImeIntervalPPS=gps->TImeIntervalPPS();
	iLengthPPS=gps->LengthPPS();
    qStdout() << "iTImeIntervalPPS : " << iTImeIntervalPPS << Qt::endl;
    qStdout() << "iLengthPPS : " << iLengthPPS << Qt::endl;
	payload.appendHeader();
	payload.append(0x06);  // TP5
	payload.append(0x31);
	payload.append(0x20);  // 32
	payload.append(0x00);
	payload.append(0x00);  // TimePulse1
	payload.append(0x01);  // Version
	payload.append(0x00);  // Reserved
	payload.append(0x00);  // Reserved
	payload.appendInt16(DelayCable);
	payload.appendInt16(0);  //RF group Delay
	payload.appendInt32(iTImeIntervalPPS); //Freq
	payload.appendInt32(iTImeIntervalPPS); //FreqWhenLocked
	payload.appendInt32(iLengthPPS); //pulseLenRatio
	payload.appendInt32(iLengthPPS); //pulseLenRatioLocked
	payload.appendInt32(0); //User Delay
	payload.append(0b11110111); //flags //https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_(UBX-13003221)_Public.pdf page 241/409
	payload.append(0x00);  // Reserved
	payload.append(0x00);  // Reserved
	payload.append(0x00);  // Reserved
	payload.appendChecksum();

	if (m_device != Q_NULLPTR) {
		m_device->write(payload.data(), payload.length());
	}

	delete(gps);
}



bool Ublox::clearFixedConfiguration()
{
	QFileInfo file(Ublox::fixedFilePath);
	if (file.exists() && file.isFile()) {
		return QFile::remove(Ublox::fixedFilePath);
	}

	return true;
}

bool Ublox::updateFixedConfiguration(int x, int y, int z, int v)
{
	clearFixedConfiguration();

	QFile file(Ublox::fixedFilePath);
	if (file.open(QIODevice::WriteOnly | QIODevice::Truncate) == false) {
		return false;
	}

	QTextStream out(&file);
    out << x << Qt::endl << y << Qt::endl << z << Qt::endl << v << Qt::endl;

	file.close();
	return true;
}

void Ublox::enterSurveyInMode()
{
	enterSurveyInMode(m_duration, m_accuracy);
}

void Ublox::enterSurveyInMode(unsigned int duration, unsigned int accuracy)
{
	UbloxFrame payload;

    qStdout() << "Enter for survey mode for " << duration << " seconds minimum and accuracy of " << accuracy << " mm2" << Qt::endl;

	// Send TMODE2 (Survey IN)
	payload.appendHeader();
	payload.append(0x06);  // TMODE 2
	payload.append(0x3D);
	payload.append(0x1C);  // 28
	payload.append(0x00);
	payload.append(0x01);  // Survey IN
	payload.append(0x00);
	payload.append(0x00);
	payload.append(0x00);
	payload.appendInt32(0);
	payload.appendInt32(0);
	payload.appendInt32(0);
	payload.appendInt32(0);
	payload.appendInt32(duration);
	payload.appendInt32(accuracy);
	payload.appendChecksum();

	if (m_device != Q_NULLPTR) {
		m_device->write(payload.data(), payload.length());
	}
}

void UbloxFrame::append(int value)
{
	this->push_back(value & 0x000000ff);
}

void UbloxFrame::appendInt32(int value)
{
	this->push_back(value & 0x000000ff);
	this->push_back((value & 0x0000ff00) >> 8);
	this->push_back((value & 0x00ff0000) >> 16);
	this->push_back((value & 0xff000000) >> 24);
}


void UbloxFrame::appendInt16(int value)
{
	this->push_back(value & 0x000000ff);
	this->push_back((value & 0x0000ff00) >> 8);
}
void UbloxFrame::appendHeader()
{
	this->push_back(0xB5);
	this->push_back(0x62);
}

void UbloxFrame::appendChecksum()
{
	int8_t chkA, chkB;
	int i, size;

	size = this->size();
	if (size <= 2) {
		return;
	}

	// Checksum
	chkA = 0;
	chkB = 0;
	for(i=2; i<size; i++) {
		chkA = chkA + (uint8_t)this->at(i);
		chkB = chkB + chkA;
	}

	this->push_back(chkA);
	this->push_back(chkB);
}

UbloxFrame UbloxFrame::parseUbxLine(QString line)
{
	UbloxFrame out;

	QStringList chunks = line.split('-');
	if (chunks.size() != 3) {
		return out;
	}

	// Ubx header
	out.appendHeader();

	// Ubx payload
	QStringList hexa = chunks[2].split(' ');
	foreach (QString bin, hexa) {
		if (bin.size() == 0) {
			continue;
		}

		out.append(bin.toInt(Q_NULLPTR, 16));
	}

	// Ublx chk
	out.appendChecksum();

	return out;
}
