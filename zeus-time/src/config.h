#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QJsonObject>

class Config {
public:
    explicit Config();
    bool load(QString filepath);
    void show();

    QString         getBrokerAddress();
    int             getBrokerPort();

    QString         getTimeSource();
	QString         getModelGPS();
    QJsonObject     getTimeConfig();
	QString			getNTPSource();


private:
    QJsonObject m_json;
};

#endif // CONFIG_H
