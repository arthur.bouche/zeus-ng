#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

class Config {
public:
    explicit Config();
    bool load(QString filepath);
    void show();

    QString getBrokerAdress();
    int getBrokerPort();
    int getPeriodLoad();
    int getPeriodUpload();
    int getSizeMaxLogFile();
    QString getFolderLoad();
    QString getFolderUpload();
    QStringList getUnits();


private:
    QJsonObject m_json;
};

#endif // CONFIG_H
