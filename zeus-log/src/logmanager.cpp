#include "logmanager.h"
#include "debug.h"


LogManager::LogManager(Config conf)
{
    m_conf = conf;
//    m_running = false;
}

LogManager::~LogManager()
{
//    m_running = false;
    foreach(LogService *service, listLogService)
    {
        service->stop();
    }
}

void LogManager::stop()
{
    foreach(LogService *service, listLogService)
    {
        service->stop();
    }
    emit stopSignal();
}

void LogManager::start()
{
    /*
     *  Open dir
     */
    //QString pathDir = "/lcpc/fileLog/";
    QString pathDir = m_conf.getFolderLoad();
    QDir uploadDir(pathDir);
    if(!uploadDir.exists())
    {
        qStdout() << "Create dir : " << pathDir << Qt::endl;
        uploadDir.mkdir(pathDir);
    }
    foreach (QString service, m_conf.getUnits())
    {
        LogService *logService;
        logService = new LogService(service, m_conf.getPeriodUpload()*1000, m_conf.getPeriodLoad()*1000, m_conf.getFolderUpload(), pathDir, m_conf.getSizeMaxLogFile());
        logService->start();
        listLogService.push_back(logService);
    }
    // m_running = true;
    // while(m_running)
    // {
    //     ZeusBusTimeout zeusTimeout;
    //     zeusTimeout.wait(5000);
    // }
}
