#ifndef LOGMANAGER_H
#define LOGMANAGER_H
#include <QObject>
#include <config.h>
//#include <zeus/zeusbustimeout.h>
#include <logservice.h>
#include <QDir>

class LogManager : public QObject
{
    Q_OBJECT
public:
    LogManager(Config conf);
    ~LogManager();
    void start();
    void stop();

signals:
    void stopSignal();

private :
    Config m_conf;
    QList<LogService*> listLogService;
    bool m_running;
};

#endif // LOGMANAGER_H
